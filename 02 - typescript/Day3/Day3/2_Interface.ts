// let area1 = function (h: number, w?: number) {
//     w = w || h;
//     return h * w;
// }

// console.log(area1(10, 20));
// console.log(area1(10));

//-------------------------------------------------- Complex Object
// let area2 = function (rect: { h: number, w?: number }) {
//     rect.w = rect.w || rect.h;
//     return rect.h * rect.w;
// }

// var r1 = { h: 10, w: 20 };
// console.log(area2(r1));

// var r2 = { h: 10 };
// console.log(area2(r2));

//----------------------------------------------------- Interface
// interface Rectangle {
//     h: number;
//     w?: number;
// }

// let area3 = function (rect: Rectangle) {
//     return rect.h * rect.w;
// }

// var s1: Rectangle = { h: 10, w: 20 };
// console.log(area3(s1));

// var s2: Rectangle = { h: 100 };
// console.log(area3(s1));

//--------------------------------------------------------- Interface With Methods
interface IPerson {
    name: string;
    age: number;
    greet: (msg: string) => string;
}

var p1: IPerson = {
    name: 'Manish',
    age: 34,
    greet: function (msg: string) {
        return "Hi There";
    }
};

var p2: IPerson = {
    name: 'Alex',
    age: 35,
    greet: function (m: string) {
        return "CYa";
    }
};

class Person implements IPerson {
    name: string;
    age: number;

    greet(m: string): string {
        return "Hello";
    }
}

var p11 = new Person();

var p12 = new Person();
