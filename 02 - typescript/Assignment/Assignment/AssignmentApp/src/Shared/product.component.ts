import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";

@Component({
    selector: 'product',
    templateUrl: 'product.component.html'
})

export class ProductComponent implements OnInit {
    private fb: FormBuilder;
    productForm: FormGroup;

    constructor(fb: FormBuilder) {
        this.fb = fb;
    }

    ngOnInit() {
        this.productForm = this.fb.group({
            name: [null, Validators.required],
            description: [null, Validators.required],
            status: [null, Validators.required]
        });
    }

    logForm() {
        console.log(this.productForm.value);
    }
}