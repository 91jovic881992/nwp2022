import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";

import { HelloComponent } from "./hello.component";
import { HelloComponent2 } from "./components/special-comp/hello2.component";

@NgModule({
    imports: [BrowserModule],
    declarations: [HelloComponent, HelloComponent2],
    bootstrap: [HelloComponent]
})
export class AppModule { } 