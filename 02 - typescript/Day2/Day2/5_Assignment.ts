// function Reverse(s: string): string[];
// function Reverse(arr: string[]): string[];

// function Reverse(): string[] {
//     function M1(x: string): string[] {
//         return x.split("").reverse();
//     }

//     function M2(arr: string[]): string[] {
//         return arr.slice().reverse();
//     }

//     if (typeof arguments[0] == "string")
//         return M1(arguments[0]);
//     else
//         return M2(arguments[0]);
// }

// function Reverse(s: string): string[];
// function Reverse(arr: string[]): string[];

// function Reverse(StrorArr: any): string[] {
//     return typeof StrorArr == "string"
//         ? StrorArr.split("").reverse()
//         : StrorArr.slice().reverse();
// }

// console.log(Reverse("Manish"));
// console.log(Reverse(["Manish", "Alex", "Ram"]));

// -------------------------------------------------------- Type Guards
// let myData: (string | number);
// myData = "ANC";
// myData = 10;

function ReverseTG(s: string): string[];
function ReverseTG(arr: string[]): string[];

function ReverseTG(StrorArr: (string | string[])): string[] {
    return typeof StrorArr == "string"
        ? StrorArr.split("").reverse()
        : StrorArr.slice().reverse();
}

console.log(ReverseTG("Manish"));
console.log(ReverseTG(["Manish", "Alex", "Ram"]));
