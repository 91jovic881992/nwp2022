// // 1. Template Inline Style
// import { Component, ViewEncapsulation } from '@angular/core';

// @Component({
//     selector: 'ctwo',
//     template: `
//             <style>
//                 .card {
//                     border-style: solid;
//                     border-width: 2px;
//                     border-color: blue;
//                 }
//             </style>
//             <h2>Template Inline Style</h2>
//             <h3 style="border-style: solid;border-width: 2px;border-color:blue">Just for Fun</h3>
//             <h3 class="card">Just for Fun</h3>
//         `,
//     encapsulation: ViewEncapsulation.Emulated
// })
// export class ComponentTwo { }

// // 2. Component Inline Style
// import { Component, ViewEncapsulation } from '@angular/core';

// @Component({
//     selector: 'ctwo',
//     styles: [
//         `.card {
//             border-style: solid;
//             border-width: 2px;
//             border-color: blue;
//         }`
//     ],
//     template: `
//             <h2>Component Inline Style</h2>
//             <h3 class="card">Just for Fun</h3>
//         `,
//     encapsulation: ViewEncapsulation.Emulated
// })
// export class ComponentTwo { }

// // 3. External CSS using Styles
// import { Component, ViewEncapsulation } from '@angular/core';

// @Component({
//     selector: 'ctwo',
//     styles: [require('./ctwo.component.css').toString()],
//     template: `
//             <h2>Component Inline Style</h2>
//             <h3 class="cardc2">Just for Fun</h3>
//         `,
//     encapsulation: ViewEncapsulation.Emulated
// })
// export class ComponentTwo { }

// 4. External CSS using StyleUrls
import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'ctwo',
    styleUrls: ['./ctwo.component.css'.toString()],
    template: `
            <h2>Component Inline Style</h2>
            <h3 class="cardc2">Just for Fun</h3>
            <h3 class="same">Using CSS Class</h3>
        `,
    encapsulation: ViewEncapsulation.Emulated
})
export class ComponentTwo { }
