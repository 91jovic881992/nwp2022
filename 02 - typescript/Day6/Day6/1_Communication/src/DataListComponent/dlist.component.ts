import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'dlcomp',
    templateUrl: 'dlist.component.html'
})

export class DataListComponent implements OnInit {
    @Input() personList: Array<string>;
    selectedPerson: string;

    constructor() { }

    ngOnInit() {
    }

    select(e: Event, p: string) {
        this.selectedPerson = p;
        e.preventDefault();
    }
}

// import { Component, OnInit } from '@angular/core';

// @Component({
//     selector: 'dlcomp',
//     templateUrl: 'dlist.component.html',
//     inputs: ['personList']
// })

// export class DataListComponent implements OnInit {
//     personList: Array<string>;
//     selectedPerson: string;

//     constructor() { }

//     ngOnInit() {
//     }

//     select(e: Event, p: string) {
//         this.selectedPerson = p;
//         e.preventDefault();
//     }
// }