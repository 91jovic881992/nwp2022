import { NgModule } from '@angular/core';
import { BrowserModule } from "@angular/platform-browser";

import { QuoteComponent } from "./QuoteComponent/quote.component";

@NgModule({
    imports: [BrowserModule],
    declarations: [QuoteComponent]
})
export class QuoteModule { }