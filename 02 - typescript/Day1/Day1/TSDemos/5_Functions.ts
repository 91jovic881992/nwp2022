//  HOISTING

// i = 10;
// console.log(i);

// var i;

// Function Declaration Syntax

// Show();

// function Show() {
//     console.log("Show Completed...");
// }

// Function Expression Syntax

// var Show1 = function(){
//     console.log("Show Completed...");
// };

// Show1();

// Function Constructor Syntax
// var Show2 = new Function('console.log("Show Completed...");');
// Show2();

function Show(): void {
    console.log("Show Completed...");
}

function Add1(a: number, b: number): number {
    return a + b;
}

var Add2 = function (a: number, b: number): number {
    return a + b;
};

var Add3: (a: number, b: number) => number;
Add3 = function (x: number, y: number) {
    return x + y;
}

//Anonymous Function
var Add4: (a: number, b: number) => number;
Add4 = function (x, y) {
    return x + y;
}

//Lambdas
var Add5: (a: number, b: number) => number;
Add5 = (x, y) => x + y;

var Add6: (a: number, b: number) => number;
Add6 = (x, y) => { return x + y };

function ClickHandler(e: Event) {
    console.log("Clicked...");
}

document.getElementById("btn").addEventListener("click", ClickHandler);

document.getElementById("btn").addEventListener("click", function(e) {
    console.log("Clicked...");
});

document.getElementById("btn").addEventListener("click", (e) => {
    console.log("Clicked...");
});