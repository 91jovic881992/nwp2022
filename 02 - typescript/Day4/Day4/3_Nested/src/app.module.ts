import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";

import { RootComponent } from "./root.component";
import { Hello1Component } from "./hello1.component";
import { Hello2Component } from "./hello2.component";

@NgModule({
    imports: [BrowserModule],
    declarations: [RootComponent, Hello1Component, Hello2Component],
    bootstrap: [RootComponent]
})
export class AppModule { } 