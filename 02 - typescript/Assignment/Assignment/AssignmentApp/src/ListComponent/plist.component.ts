import { Component, OnInit } from '@angular/core';

import { ProductsService } from "../Services/products.service";

@Component({
    selector: 'plist',
    templateUrl: 'plist.component.html',
    providers: [ProductsService]
})

export class PListComponent implements OnInit {
    products: any[];

    constructor(private dService: ProductsService) { }

    ngOnInit() {
        this.dService.getProducts().subscribe(results => {
            this.products = results;
        });
    }
}