import { Component } from '@angular/core';
import { Author } from '../Models/app.author';

import { DataService } from "../Services/data.service";

@Component({
    selector: 'alist',
    templateUrl: 'list.component.html',
    styles: [require('./list.component.css').toString()]
})
export class ListComponent {
    list: Array<Author>;
    selectedAuthor: Author;

    constructor(private dService: DataService) { }

    ngOnInit() {
        this.list = this.dService.getAllAuthors();
    }

    selectAuthor(a: Author) {
        this.dService.Author = a;
        this.selectedAuthor = this.dService.Author;
    }

    isSelected(a: Author) {
        return this.selectedAuthor === a;
    }
}