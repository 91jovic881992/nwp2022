import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { FormsModule } from "@angular/forms";

import { RootComponent } from "./root.component";
import { ListComponent } from "./ListComponent/list.component";
import { DataListComponent } from "./DataListComponent/dlist.component";
import { CounterComponent } from "./CounterComponent/counter.component";

@NgModule({
    imports: [BrowserModule, FormsModule],
    declarations: [RootComponent, CounterComponent, ListComponent, DataListComponent],
    bootstrap: [RootComponent]
})
export class AppModule { } 