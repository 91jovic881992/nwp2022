// function Person(a: number) {
//     this.age = a;
//     this.growOld = function () {
//         console.log(this);
//         this.age += 1;
//     }
// }

// function Person(a: number) {
//     var self = this;
//     self.age = a;
//     self.growOld = function () {
//         self.age += 1;
//     }
// }

function Person(a: number) {
    this.age = a;
    this.growOld = () => {
        this.age += 1;
    }
}

var p = new Person(20);

console.log('ageeeeeeee: ' + p.age);
p.growOld();
p.growOld();
p.growOld();
p.growOld();
console.log('ageeeeeeee ' + p.age);

setInterval(function () { 
    p.growOld();
    console.log(p.age);
}, 1000);

setInterval(p.growOld, 1000);
// setInterval(p.growOld.bind(p), 1000);

setInterval(function () { 
    console.log(p.age);
}, 1000);