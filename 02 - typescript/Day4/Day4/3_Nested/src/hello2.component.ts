import { Component } from "@angular/core";

@Component({
    selector: 'hello2',
    template: '<h2>Hello World Again! - C2</h2>'
})
export class Hello2Component { }