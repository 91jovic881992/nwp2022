function Hello(): void;
function Hello(name: string): void;

function Hello() {
    function M1() {
        console.log("Hello World!");
    }

    function M2(name: string) {
        console.log("Hello, ", name);
    }

    if (arguments.length == 0)
        M1();
    else if (arguments.length == 1)
        M2(arguments[0]);
    else
        throw "No Implementation Found...";
};

Hello();
Hello("Manish");