// // 1. Template Inline Style
// import { Component, ViewEncapsulation } from '@angular/core';

// @Component({
//     selector: 'cone',
//     template: `
//             <style>
//                 .card {
//                     border-style: solid;
//                     border-width: 2px;
//                     border-color: red;
//                 }
//             </style>
//             <h2>Template Inline Style</h2>
//             <h3 style="border-style: solid;border-width: 2px;border-color:red">Just for Fun</h3>
//             <h3 class="card">Just for Fun</h3>
//         `,
//     encapsulation: ViewEncapsulation.Emulated
// })
// export class ComponentOne { }

// // 2. Component Inline Style
// import { Component, ViewEncapsulation } from '@angular/core';

// @Component({
//     selector: 'cone',
//     styles: [
//         `.card {
//             border-style: solid;
//             border-width: 2px;
//             border-color: red;
//         }`
//     ],
//     template: `
//             <h2>Component Inline Style</h2>
//             <h3 class="card">Just for Fun</h3>
//         `,
//     encapsulation: ViewEncapsulation.Emulated
// })
// export class ComponentOne { }

// // 3. External CSS using Styles
// import { Component, ViewEncapsulation } from '@angular/core';

// @Component({
//     selector: 'cone',
//     styles: [require('./cone.component.css').toString()],
//     template: `
//             <h2>Component Inline Style</h2>
//             <h3 class="cardc1">Just for Fun</h3>
//         `,
//     encapsulation: ViewEncapsulation.Emulated
// })
// export class ComponentOne { }

// 4. External CSS using StyleUrls
import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'cone',
    styleUrls: ['./cone.component.css'.toString()],
    template: `
            <h2>Component Inline Style</h2>
            <h3 class="cardc1">Just for Fun</h3>
            <h3 class="same">Using CSS Class</h3>
        `,
    encapsulation: ViewEncapsulation.Emulated
})
export class ComponentOne { }