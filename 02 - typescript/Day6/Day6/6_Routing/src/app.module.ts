import { NgModule } from '@angular/core';
import { BrowserModule } from "@angular/platform-browser";
import { FormsModule } from '@angular/forms'
import { RouterModule } from "@angular/router";

import { QuoteModule } from "./quote.module";

import { DefaultComponent } from "./default.component";
import { RootComponent } from "./root.component";
import { ListComponent } from "./ListComponent/list.component";
import { QuoteComponent } from "./QuoteComponent/quote.component";

import { routes } from './app.routes';

@NgModule({
    imports: [BrowserModule, FormsModule, QuoteModule, RouterModule.forRoot(routes)],
    declarations: [RootComponent, ListComponent, DefaultComponent],
    bootstrap: [RootComponent]
})
export class AppModule { }