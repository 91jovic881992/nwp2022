import { Component, ViewChild } from "@angular/core";

@Component({
    selector: 'root',
    template: `
            <div class="container">     
                <lcomp></lcomp>
            </div>
        `
})
export class RootComponent {
}