import { Component } from "@angular/core";

@Component({
    selector: 'hello2',
    template: '<h2>Hello World! - C2 - From ModuleTwo</h2>'
})
export class Hello2Component { }