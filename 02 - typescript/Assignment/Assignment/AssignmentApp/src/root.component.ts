import { Component } from "@angular/core";

@Component({
    selector: 'root',
    template: `
            <div class="container">
                <nav class="navbar navbar-light">
                    <ul class="nav navbar-nav">
                        <li><a class="active">Home</a></li>
                        <li><a>About</a></li>
                        <li><a routerLink="/">Products</a></li>
                    </ul>
                </nav>
                <router-outlet></router-outlet>
            </div>
        `
})
export class RootComponent {
}