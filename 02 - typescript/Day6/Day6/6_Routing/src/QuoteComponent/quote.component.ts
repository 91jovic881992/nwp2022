import { Component } from '@angular/core';
import { ActivatedRoute } from "@angular/router";

import { Author } from '../Models/app.author';

import { DataService } from "../Services/data.service";

@Component({
    selector: 'squote',
    templateUrl: 'quote.component.html',
    styles: [require('./quote.component.css').toString()]
})
export class QuoteComponent {
    selectedAuthor: Author;
    name: string;

    constructor(private dService: DataService, private route: ActivatedRoute) { }

    ngOnInit() {
        this.route.params.map(p => p['name']).subscribe(n => { this.name = n });
    }

    get() {
        this.selectedAuthor = this.dService.Author;
    }
}