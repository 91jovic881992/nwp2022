// class Employee {
//     private name: string;
//     // constructor(n: string) {
//     //     this.name = n;
//     // }
//     constructor(n = "NA") {
//         this.name = n;
//     }
//     getName() {
//         return this.name;
//     }
//     setName(n: string) {
//         this.name = n;
//     }
// }
// var e1 = new Employee("Manish");
// console.log(e1.getName());
// e1.setName("Abhijeet");
// console.log(e1.getName());
// var e2 = new Employee();
//------------------------------------------------ Properties
// class Employee {
//     private name: string;
//     private age: number;
//     constructor(n = "NA", a = 0) {
//         this.name = n;
//         this.age = a;
//     }
//     get Name() {
//         return this.name;
//     }
//     set Name(n: string) {
//         this.name = n;
//     }
//     get Age() {
//         return this.age;
//     }
//     set Age(value: number) {
//         this.age = value;
//     }
// }
// var e1 = new Employee("Manish", 34);
// console.log(e1.Name + "\t" + e1.Age);
// e1.Name = "Abhijeet";
// e1.Age = 35;
// console.log(e1.Name + "\t" + e1.Age);
//------------------------------------------------ Parameter Members
// class Employee {
//     constructor(private name = "NA", private age = 0, public city = "NA") { }
//     get Name() {
//         return this.name;
//     }
//     set Name(n: string) {
//         this.name = n;
//     }
//     get Age() {
//         return this.age;
//     }
//     set Age(value: number) {
//         this.age = value;
//     }
// }
// var e1 = new Employee("Manish", 34);
// e1.city = "Pune";
// console.log(e1.Name + "\t" + e1.Age);
// e1.Name = "Abhijeet";
// e1.Age = 35;
// console.log(e1.Name + "\t" + e1.Age);
// ------------------------------------------------------------------- Static & Readonly
var BankAccount = /** @class */ (function () {
    function BankAccount(accNumber) {
        this.accNumber = accNumber;
    }
    Object.defineProperty(BankAccount.prototype, "BankName", {
        // private readonly accNumber: number;
        // constructor(accNumber: number) {
        //     this.accNumber = accNumber;
        // }
        get: function () {
            return BankAccount.bankName;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BankAccount.prototype, "AccNumber", {
        get: function () {
            return this.accNumber;
        },
        enumerable: true,
        configurable: true
    });
    BankAccount.bankName = "Wells";
    return BankAccount;
}());
var a1 = new BankAccount(1);
console.log(a1.AccNumber);
console.log(a1.BankName);
// a1.BankName = 100;
var a2 = new BankAccount(2);
console.log(a2.AccNumber);
console.log(a2.BankName);
