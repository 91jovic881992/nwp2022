multiple module instancing

	kreiramo novi module u pod paketu i ucitavamo ga

production build
	package.json unutar scripst dodamo build
	npm run build
	kreiran je dist folder sa 3 .js fajla i index.html
	onda mozemo da uzmemo dist folder i da ga deployujemo na production server sam
	minimalizacija vendor js...u webpack.prod.js  plugins:[
        		new webpack.optimize.UglifyJsPlugin()
    	]

debuging
	u webpack.dev.js dodamo u webpack merge       devtool: 'cheap-module-eval-source-map',
	tsconfig.json         "sourceMap": true,


2_usingbootstrap
	npm install --save jquery bootstrap@3 (za verziju 3)  --save znaci da ide i u produkciju, -dev samo za development
	importujemo u vendor.ts		
		import 'jquery';
		import 'bootstrap/dist/js/bootstrap';
		import 'bootstrap/dist/css/bootstrap.css';

	ne moze se ucitati css, pa se u package.json treba dodati css loader...tj. treba se skinuti loader i iskonfigurisati...
		npm install --save-dev css-loader file-loader style-loader
		u webpack.common.js ucitamo loader
			 {
	                test: /\.css$/,
	                loader: 'style-loader!css-loader?root=.'
		            },
			     {
		                test: /\.(eot|svg|ttf|woff|woff2)$/,
		                loader: 'file-loader?name=public/fonts/[name].[ext]'
		            }
		Error: Bootstrap's JavaScript requires jQuery
			webpack ne stavlja jQuery u scope, pa modamo u webpack.common.js naglasiti u plugins...
			 new webpack.ProvidePlugin({
		            $: 'jquery',
            		jQuery: 'jquery',
            		"window.jquery" : 'jquery'
        		})
		stop pa npm start...izmenimo malo root.component.js


////ODVAJANJE CSS 
	odvajamo css iz vendor.js...pomocu webpacka...plugin se zove extract text plugin ili tako nesto
		npm install --save-dev extract-text-webpack-plugin optimize-css-assets-webpack-plugin

var ExtractTextPlugin = require('extract-text-webpack-plugin');

izmeniti unutar loadera

	
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: "css-loader"
                })
            },

u plugin dodati  
	        new ExtractTextPlugin('[name].css')



webpack.prod 
var OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');


		   new OptimizeCssAssetsPlugin({
            cssProcessor: require('cssnano'),
            cssProcessorOptions: { discardComments: { removeAll: true } },
            canPrint: true
        })

view encapsulation - radi reusabilitya

cssmodules, kada se naprave klase, u webpack.common.js se stavi css loader
			
            {
                test: /\.css$/,
                include: /node_modules/,
                use: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: "css-loader"
                })
            },

            {
                test: /\.css$/,
                exclude: /node_modules/,
                use: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: "css-loader"
                })
            },




DATA BINDING
	property
		source			target
		  class		     template
		    datamembers    html elements
			--------------->
				ONE-WAY DATABINDING
					4 nacina {{prop}}, ime={{prop}}, bind-ime=prop, [ime]=prop
					



	event
	twoway


two-way data binding
instalirati ngmodel
	npm install --save @angular/forms
	vendor.js
	app.module import 



Augury
