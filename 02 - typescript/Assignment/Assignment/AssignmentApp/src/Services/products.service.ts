import { Injectable } from "@angular/core";
import { Http } from "@angular/http";

@Injectable()
export class ProductsService {
    private url = "http://localhost:8000/products";

    constructor(private http: Http) { }

    getProducts() {
        return this.http.get(this.url).map((res) => res.json());
    }
}