import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { FormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";

import { RootComponent } from "./root.component";
import { ListComponent } from "./ListComponent/list.component";

@NgModule({
    imports: [BrowserModule, FormsModule, HttpModule],
    declarations: [RootComponent, ListComponent],
    bootstrap: [RootComponent]
})
export class AppModule { } 