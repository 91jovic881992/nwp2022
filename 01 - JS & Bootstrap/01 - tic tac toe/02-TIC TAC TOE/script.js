gameActive = true;
currentPlayer = "X"
gameState = ["", "", "", "", "", "", "", "", "",]

const winningConditions = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6]
]


function handleRestartGame() {
    gameActive = true;
    currentPlayer = "X"
    gameState = ["", "", "", "", "", "", "", "", "",]
    document.querySelectorAll('.cell').forEach(cell => cell.innerHTML = "")
}

function handleCellClick(clickedCellEvent) {
    const clickedCell = clickedCellEvent.target
    const clickedCellIndex = parseInt(clickedCell.getAttribute('data-cell-index'))



    if (gameState[clickedCellIndex] !=="" || !gameActive){
        return;
    }
    gameState[clickedCellIndex] = currentPlayer
    clickedCell.innerHTML = currentPlayer;

    handleResult()
    currentPlayer = currentPlayer === "X" ? "O" : "X"
    statusDisplay.innerHTML = currentPlayer + " is on move"
}

function handleResult() {
    let roundWon = false;
    for (let i = 0; i < 8; i++) {
        const winningCondition = winningConditions[i];
        let a = gameState[winningCondition[0]]
        let b = gameState[winningCondition[1]]
        let c = gameState[winningCondition[2]]
        if (a === '' || b === '' || c === '') {
            continue;
        }
        if (a === b && b === c) {
            roundWon = true;
            break;
        }
    }
    if (roundWon){
        gameActive = false;
        statusDisplay.innerHTML= currentPlayer + " has won"
        return;
    }

    let roundDraw = !gameState.includes("");
    if (roundDraw){
        gameActive = false;
        statusDisplay.innerHTML = "Draw...."
        return;
    }
}


const statusDisplay = document.querySelector('.game-status')
document.querySelectorAll('.cell').forEach(cell => cell.addEventListener('click', handleCellClick))
document.querySelector('.restart').addEventListener('click', handleRestartGame)