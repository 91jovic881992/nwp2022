import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'lcomp',
    templateUrl: 'list.component.html'
})

export class ListComponent implements OnInit {
    personList: Array<string>;
    selectedPerson: string;

    constructor() { }

    ngOnInit() {
        this.personList = ["Alex", "Manish", "Mario", "Abhijeet", "Ram"];
    }

    select(e: Event, p: string) {
        this.selectedPerson = p;
        e.preventDefault();
    }
}