// class Employee {
//     name: string;
//     constructor(n: string) {
//         this.name = n;
//     }

//     getName() {
//         return this.name;
//     }

//     setName(n: string) {
//         this.name = n;
//     }
// }

// var e1 = new Employee("Manish");
// console.log(e1.getName());
// e1.setName("Alex");
// console.log(e1.getName());

// var e2 = new Employee("Abhijeet");
// console.log(e2.getName());
// e2.setName("Ramakant");
// console.log(e2.getName());

class Employee {
    name: string;

    constructor(n: string) {
        this.name = n;
    }

    getName() {
        return this.name;
    }

    setName(n: string) {
        this.name = n;
    }

}

var e1 = new Employee("Raske");
console.log(e1.getName());
e1.setName("NeRaske");
console.log(e1.getName());

var e2 = new Employee("Mika");
console.log(e1.getName());
e1.setName("Zika");
console.log(e1.getName());