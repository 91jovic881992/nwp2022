import { Component, OnInit, OnDestroy } from '@angular/core';
import { Author } from '../Models/app.author';

import { DataService } from "../Services/data.service";
import { Subscription } from 'rxjs';

@Component({
    selector: 'squote',
    templateUrl: 'quote.component.html',
    styles: [require('./quote.component.css').toString()]
})
export class QuoteComponent implements OnInit, OnDestroy {
    selectedAuthor: Author;
    sub: Subscription;

    constructor(private dService: DataService) { }

    ngOnInit() {
        this.sub = this.dService.sAuthorChanged.subscribe(() => {
            if (this.dService.Author)
                this.selectedAuthor = this.dService.Author;
        });
    }

    ngOnDestroy(){
        this.sub.unsubscribe();
    }
}