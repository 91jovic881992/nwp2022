import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";

import { RootComponent } from "./root.component";
import { ComponentOne } from "./ComponentOne/cone.component";
import { ComponentTwo } from "./ComponentTwo/ctwo.component";

@NgModule({
    imports: [BrowserModule],
    declarations: [RootComponent, ComponentOne, ComponentTwo],
    bootstrap: [RootComponent]
})
export class AppModule { } 