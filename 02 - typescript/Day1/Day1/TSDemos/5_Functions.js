//  HOISTING
// i = 10;
// console.log(i);
// var i;
// Function Declaration Syntax
// Show();
// function Show() {
//     console.log("Show Completed...");
// }
// Function Expression Syntax
// var Show1 = function(){
//     console.log("Show Completed...");
// };
// Show1();
// Function Constructor Syntax
// var Show2 = new Function('console.log("Show Completed...");');
// Show2();
function Show() {
    console.log("Show Completed...");
}
function Add1(a, b) {
    return a + b;
}
var Add2 = function (a, b) {
    return a + b;
};
var Add3;
Add3 = function (x, y) {
    return x + y;
};
//Anonymous Function
var Add4;
Add4 = function (x, y) {
    return x + y;
};
//Lambdas
var Add5;
Add5 = function (x, y) { return x + y; };
var Add6;
Add6 = function (x, y) { return x + y; };
function ClickHandler(e) {
    console.log("Clicked...");
}
document.getElementById("btn").addEventListener("click", ClickHandler);
document.getElementById("btn").addEventListener("click", function (e) {
    console.log("Clicked...");
});
document.getElementById("btn").addEventListener("click", function (e) {
    console.log("Clicked...");
});
