import { Component } from "@angular/core";

@Component({
    selector: 'hello1',
    template: '<h2 class="text-success">Hello World! - C1</h2>'
})
export class Hello1Component { }