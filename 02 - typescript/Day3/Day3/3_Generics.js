// var arr = new Array<number>();
// class Queue {
//     private data: any[] = [];
//     push(d: any) {
//         this.data.push(d);
//     }
//     pop() {
//         return this.data.shift();
//     }
// }
var Queue = /** @class */ (function () {
    function Queue() {
        this.data = [];
    }
    Queue.prototype.push = function (d) {
        this.data.push(d);
    };
    Queue.prototype.pop = function () {
        return this.data.shift();
    };
    return Queue;
}());
var q1 = new Queue();
q1.push(10);
console.log(q1.pop().toFixed());
var q2 = new Queue();
q2.push("ABC");
console.log(q2.pop().toUpperCase());
function GetLength(arg) {
    return arg.length;
}
console.log(GetLength("Manish"));
// console.log(GetLength(10));
console.log(GetLength([10, 20, 30, 40, 50, 60]));
