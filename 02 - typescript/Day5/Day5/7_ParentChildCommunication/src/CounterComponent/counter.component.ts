import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'counter',
    templateUrl: 'counter.component.html'
})

export class CounterComponent implements OnInit {
    private count: number;
    private flag: boolean;
    private clickCount: number;

    @Input() interval: number;

    constructor() { 
        // this.interval = 1;
    }

    ngOnInit() {
        this.reset();
    }

    private manageClickCount() {
        this.clickCount += 1;
        if (this.clickCount > 9) {
            this.flag = true;
        }
    }

    private inc() {
        this.manageClickCount();
        this.count += this.interval;
    }

    private dec() {
        this.manageClickCount();
        this.count -= this.interval;
    }

    reset(){
        this.interval = this.interval || 1;
        this.count = 0;
        this.clickCount = 0;
        this.flag = false;
    }
}