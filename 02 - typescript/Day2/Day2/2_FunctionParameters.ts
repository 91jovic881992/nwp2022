// function Add(x: number, y: number) {
//     return x + y;
// }

// console.log(Add(2,3));
// console.log(Add(2));

// Optional Parameters
// function Add(x: number, y?: number) {
//     y = y || 0;
//     return x + y;
// }

// console.log(Add(2, 3));
// console.log(Add(2));

// Default Parameters
function Add(x: number, y = 0) {
    return x + y;
}

console.log(Add(2, 3));
console.log(Add(2));