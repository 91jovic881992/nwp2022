import { Component, OnInit } from '@angular/core';
import { Product } from '../model/product';

@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.scss']
})
export class ProductItemComponent implements OnInit {

  //deklasati product
  public product: Product;
  public product2: Product;
  public product3: Product;

  public quantities: Array<number>;

  public products: Array<Product>;

  //inicijalizovati sa default vrednostima
  constructor() {
    this.quantities=[]
    for (let i = 1; i <=20; i++){
      this.quantities.push(i)
    }

    this.product = {
      name: "Dog food",
      imageURL: "https://vcahospitals.com/-/media/2/VCA/Images/LifeLearn-Images-Foldered/17507/Dog_Food_Choices.ashx",
      price: 300,
      isOnSale: true,
      quantity: 0
    }
    this.product2 = {
      name: "Dog food 2",
      imageURL: "https://i5.walmartimages.ca/images/Large/099/456/6000199099456.jpg",
      price: 500,
      isOnSale: false,
      quantity: 0
    }
    this.product3 = {
      name: "Dog food 3",
      imageURL: "https://cdn.shopify.com/s/files/1/0086/0795/7054/products/RoyalCaninMaxiBreedJuniorDryPuppyFood_large.jpg?v=1636366212",      
      price: 400,
      isOnSale: true,
      quantity: 0
    }

    this.products = []
    this.products.push(this.product)
    this.products.push(this.product2)
    this.products.push(this.product3)
  }

  ngOnInit(): void {
  }


  increaseQuantity(i:number){
    this.products[i].quantity++;
  }

  decreaseQuantity(i:number){
    if (this.products[i].quantity > 0){
      this.products[i].quantity--
    }
  }

  setQuantity(val:number, i:number){
    this.products[i].quantity=val
  }

  // setQuantity(val:any){
  //   this.product.quantity=val.value
  // }
}
