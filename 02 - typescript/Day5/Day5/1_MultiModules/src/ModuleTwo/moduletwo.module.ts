import { NgModule } from '@angular/core';

import { Hello2Component } from './hello2.component';

@NgModule({
    exports: [Hello2Component],
    declarations: [Hello2Component],
})
export class ModuleTwo { }
