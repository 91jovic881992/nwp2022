// Rest Parameters
// function Hello(fname: string, ...args: any[]) {
//     console.log("Hello, ", fname);
//     console.log(args);
// }

// Hello("Manish");
// Hello("Manish", "Sharma");
// Hello("Manish", "Sharma", "Pune");

function Calculate(...args: number[]) {
    var sum = 0;
    for (let i = 0; i < args.length; i++) {
        sum += args[i];
    }
    return sum;
}

// console.log(Calculate());
// console.log(Calculate(1));
// console.log(Calculate(1, 2));
// console.log(Calculate(1, 2, 3, 4, 5));

let arr = [10, 20, 30, 40, 50];
let arr1 = new Array();

// console.log(Calculate(arr[0],arr[1]));
// console.log(Calculate(...arr));         //Spread Operator

console.log(arr);
console.log(...arr);
