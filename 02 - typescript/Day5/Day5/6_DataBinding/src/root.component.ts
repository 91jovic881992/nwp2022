import { Component, ChangeDetectionStrategy, AfterViewInit } from "@angular/core";

@Component({
    selector: 'root',
    template: `
            <div class="container">     
                <h1 class="text-danger">Data Binding</h1>
                <h2>Two Way Binding</h2>
                One Way: <input type="text" [value]=message>
                <br/>
                Two Way: <input type="text" bindon-ngModel=message>
                <br/>
                Two Way: <input type="text" [(ngModel)]=message>
                <br/>
                Implicit Model
                <input type="text" [(ngModel)]=city (change)=update(city)>
                <input type="text" [(ngModel)]=city (input)=update(city)>
                <input type="text" [(ngModel)]=city [ngModelOptions]="{updateOn: 'blur'}">

                <h4>City Name: {{city}}</h4>

                <hr/>
                <h2>Event Binding</h2>
                <button class="btn btn-primary" on-click=doClick()>Click Me!</button>
                <button class="btn btn-primary" (click)=doClick()>Click Me!</button>
                <button class="btn btn-danger" id="btnJS">Click Me!</button>

                <hr/>
                <h2>Property Binding</h2>
                <clock></clock>
                <h3>Message: {{message}}</h3>
                <h3 innerHTML={{message}}>Message: </h3>
                <h3 bind-innerHTML=message>Message: </h3>
                <h3 [innerHTML]=message>Message: </h3>
                <input type="text" [value]=message>

                <button class="btn btn-primary" disabled={{flag}}>Click Me!</button>
                <button class="btn btn-primary" bind-disabled=flag>Click Me!</button>
                <button class="btn btn-primary" [disabled]=flag>Click Me!</button>
            </div>
        `,
    // changeDetection: ChangeDetectionStrategy.OnPush
})
export class RootComponent implements AfterViewInit {
    message: string;
    flag: boolean;

    constructor() {
        this.message = "Hello World!";
        this.flag = true;
    }

    ngAfterViewInit() {
        document.getElementById("btnJS").addEventListener("click", () => {
            // alert("Hi There");
            this.message = "Changed using JS..." + new Date().toLocaleTimeString();
        });
    }

    doClick() {
        // alert("Hello");
        this.message = "Changed... " + new Date().toLocaleTimeString();
    }

    update(city: string) {
        this.message = city;
    }
}