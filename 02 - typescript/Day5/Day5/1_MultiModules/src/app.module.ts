import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";

import { ModuleTwo } from "./ModuleTwo/moduletwo.module";

import { RootComponent } from "./root.component";
import { Hello1Component } from "./hello1.component";

@NgModule({
    imports: [BrowserModule, ModuleTwo],
    declarations: [RootComponent, Hello1Component],
    bootstrap: [RootComponent]
})
export class AppModule { } 