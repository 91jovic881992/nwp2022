//Implicitly Typed

// var data = 10;
// data = "ABC";

// var data;
// data = 10;
// data = "ABC";

// var x = "ABC";

//Explicitly Typed

// var age: number;
// age = 10;
// // age = true;

// function add(x: number, y: number) {
//     return x + y;
// }

// console.log(add(2, 3));
// // console.log(add(2, "ABC"));
// // console.log(add(2, true));
// console.log(add(2, undefined));