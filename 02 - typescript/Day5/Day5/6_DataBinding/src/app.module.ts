import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { FormsModule } from "@angular/forms";

import { RootComponent } from "./root.component";
import { ClockComponent } from "./ClockComponent/clock.component";

@NgModule({
    imports: [BrowserModule, FormsModule],
    declarations: [RootComponent, ClockComponent],
    bootstrap: [RootComponent]
})
export class AppModule { } 