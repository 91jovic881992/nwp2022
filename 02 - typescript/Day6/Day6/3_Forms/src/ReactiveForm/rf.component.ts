// 3. Validations

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";

@Component({
    selector: 'rf',
    templateUrl: 'rf.component.html'
})

export class ReactiveFormComponent implements OnInit {
    registrationForm: FormGroup;

    constructor(private fb: FormBuilder) { }

    ngOnInit() {
        this.registrationForm = this.fb.group({
            firstname: [null, Validators.required],
            lastname: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(10)])],
            address: this.fb.group({
                city: [null, Validators.required],
                zip: 0
            })
        });
    }

    logForm() {
        if (this.registrationForm.valid) {
            // Code to send data to service
            console.log(this.registrationForm.value);
        } else {
            console.error("Invalid Form Data...");
        }
    }
}

// // 2. Using FormGroup and FormBuilder

// import { Component, OnInit } from '@angular/core';
// import { FormGroup, FormBuilder } from "@angular/forms";

// @Component({
//     selector: 'rf',
//     templateUrl: 'rf.component.html'
// })

// export class ReactiveFormComponent implements OnInit {
//     registrationForm: FormGroup;

//     constructor(private fb: FormBuilder) { }

//     ngOnInit() {
//         this.registrationForm = this.fb.group({
//             firstname: "Manish",
//             lastname: "",
//             address: this.fb.group({
//                 city: "",
//                 zip: 0
//             })
//         });
//     }

//     logForm() {
//         console.log(this.registrationForm.value);
//     }
// }

// // 1. Using FormGroup and FormControl
// import { Component, OnInit } from '@angular/core';
// import { FormGroup, FormControl } from "@angular/forms";

// @Component({
//     selector: 'rf',
//     templateUrl: 'rf.component.html'
// })

// export class ReactiveFormComponent implements OnInit {
//     registrationForm: FormGroup;

//     constructor() { }

//     ngOnInit() {
//         this.registrationForm = new FormGroup({
//             firstname: new FormControl(),
//             lastname: new FormControl(),
//             address: new FormGroup({
//                 city: new FormControl(),
//                 zip: new FormControl()
//             })
//         });
//     }

//     logForm() {
//         console.log(this.registrationForm.value);
//     }
// }