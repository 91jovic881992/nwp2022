import { Component, OnInit } from '@angular/core';

import { DataService } from "../Services/data.service";

@Component({
    selector: 'lcomp',
    templateUrl: 'list.component.html',
    providers: [DataService]
})

export class ListComponent implements OnInit {
    posts: any[];

    constructor(private dService: DataService) { }

    ngOnInit() {
        this.dService.getAllPosts().subscribe(results => {
            this.posts = results;
        });
    }
}