import { Component } from '@angular/core';

var styles = require('./cone.component.css');

@Component({
    selector: 'cone',
    template: `
            <h2>CSS Modules - C1</h2>
            <h3 class="${styles.same}">Using CSS Class</h3>
        `
})
export class ComponentOne { }