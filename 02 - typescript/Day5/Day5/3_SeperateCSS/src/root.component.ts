import { Component } from "@angular/core";

@Component({
    selector: 'root',
    template: `
            <div class="container">     
                <h1 class="text-danger">Root Component Loaded....</h1>
                <hello1></hello1>
            </div>
        `
})
export class RootComponent { }