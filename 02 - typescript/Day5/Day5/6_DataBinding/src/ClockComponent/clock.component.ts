import { Component } from '@angular/core';

@Component({
    selector: 'clock',
    template: `<h5 class="text-right">Current Time: {{timeString}}</h5>`
})

export class ClockComponent {
    timeString: string;

    constructor() {
        setInterval(() => { this.startTimer(); }, 1000);
    }

    private startTimer() {
        var today = new Date();
        var h = today.getHours();
        var m = today.getMinutes();
        var s = today.getSeconds();
        m = this.addZero(m);
        s = this.addZero(s);
        this.timeString = `${h}:${m}:${s}`;
        // console.log(this.timeString);
    }

    // For adding zero in front of numbers < 10
    private addZero(i: any) {
        if (i < 10) { i = "0" + i };
        return i;
    }
}