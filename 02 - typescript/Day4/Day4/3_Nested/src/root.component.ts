import { Component } from "@angular/core";

@Component({
    selector: 'root',
    template: ` 
            <h1>Root Component Loaded....</h1>
            <hello1></hello1>
            <hello2></hello2>
            <hello1></hello1>
        `
})
export class RootComponent { }

// import { Component } from "@angular/core";

// @Component({
//     selector: 'root',
//     template: '<h1>Root Component Loaded....</h1><hello1></hello1><hello2></hello2><hello1></hello1>'
// })
// export class RootComponent { }