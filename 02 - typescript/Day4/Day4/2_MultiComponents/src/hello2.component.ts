import { Component } from "@angular/core";

@Component({
    selector: 'hello2',
    template: '<h1>Hello World Again!</h1>'
})
export class Hello2Component { }