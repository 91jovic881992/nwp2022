// function Reverse(s: string): string[];
// function Reverse(arr: string[]): string[];
function ReverseTG(StrorArr) {
    return typeof StrorArr == "string"
        ? StrorArr.split("").reverse()
        : StrorArr.slice().reverse();
}
console.log(ReverseTG("Manish"));
console.log(ReverseTG(["Manish", "Alex", "Ram"]));
