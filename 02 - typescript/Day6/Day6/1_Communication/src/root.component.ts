import { Component, ViewChild } from "@angular/core";
import { CounterComponent } from "./CounterComponent/counter.component";

@Component({
    selector: 'root',
    template: `
            <div class="container">     
                <h1 class="row">Communication between Parent and Child</h1>
                <div class="well well-sm">
                    <h4>{{message}}</h4>
                </div>
                <!-- Template Reference -->
                <counter #c1 (onMax)=maxedOut($event)></counter>
                <br/>
                <button class="btn btn-danger btn-block" (click)=c1.reset()>Parent Reset - Template Reference</button>
                <button class="btn btn-danger btn-block" (click)=reset(c1)>Parent Reset - Code</button>
                
                <!-- <counter [interval]=10></counter> -->
                <!-- <dlcomp [personList]=plist></dlcomp> -->
                <!-- <lcomp></lcomp> -->
            </div>
        `
})
export class RootComponent {
    plist: Array<string> = ["Alex", "Manish", "Mario", "Abhijeet", "Ram"];
    message: string;

    @ViewChild(CounterComponent)
    cnt: CounterComponent;

    ngAfterViewInit() {
        //console.log(this.cnt);
        this.cnt.interval = 10;
    }

    reset(c: CounterComponent) {
        c.reset();
    }

    maxedOut(flag:boolean){
        if(flag)
            this.message = "Max Click Reached...., click Reset to continue....";
        else
            this.message = ""; 
    }
}