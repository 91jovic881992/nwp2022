let currentPlayer = "X";
let activeGame = true;
let gameState = ["", "", "", "", "", "", "", "", ""]

const win = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6]
]


function handleRestartGame() {
    activeGame = true;
    currentPlayer = "X";
    gameState = ["", "", "", "", "", "", "", "", ""]
    //resetuj celije
    document.querySelectorAll('.cell').forEach(cell => cell.innerHTML ="");
}

function handleCellClick(clikedCellEvent) {
    const clickedCell = clikedCellEvent.target
    const clickedCellIndex = parseInt(clickedCell.getAttribute("data-cell-index"))
    
    if (gameState[clickedCellIndex] !=="" || !activeGame){
        return;
    }
    
    gameState[clickedCellIndex] = currentPlayer;
    clickedCell.innerHTML = currentPlayer

    handleResult()

    currentPlayer = currentPlayer === "X" ? "O" : "X";
}


function handleResult() {
    let roundWon = false;
    for (let i = 0; i < 8; i++) {
        const winCondition = win[i]
        let a = gameState[winCondition[0]]
        let b = gameState[winCondition[1]]
        let c = gameState[winCondition[2]]

        if (a === '' || b === '' || c === '') {
            continue;
        }
        if (a === b && b === c) {
            roundWon = true;
            break
        }
    }
    if (roundWon){
        alert(currentPlayer + " has won")
        activeGame = false;
        return;
    }

    let roundDraw = !gameState.includes("")
    if (roundDraw){
        alert("A draw")
        activeGame = false;
    }
}



document.querySelectorAll(".cell").forEach(cell => cell.addEventListener('click', handleCellClick))
document.querySelector(".restart").addEventListener('click', handleRestartGame)