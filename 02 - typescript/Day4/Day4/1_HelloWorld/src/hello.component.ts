import { Component } from "@angular/core";

@Component({
    selector: 'hello',
    template: '<h1>Hello World! <br><hello2></hello2></h1>'
})
export class HelloComponent { }