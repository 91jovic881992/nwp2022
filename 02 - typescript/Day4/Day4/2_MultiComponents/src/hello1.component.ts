import { Component } from "@angular/core";

@Component({
    selector: 'hello1',
    template: '<h1>Hello World!</h1>'
})
export class Hello1Component { }