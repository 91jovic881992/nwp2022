import { Component } from "@angular/core";

@Component({
    selector: 'hello1',
    template: '<h2>Hello World! - C1</h2>'
})
export class Hello1Component { }