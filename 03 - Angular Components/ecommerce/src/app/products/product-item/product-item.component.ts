import { Component, OnInit } from '@angular/core';
import { Product } from '../model/product';

@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.scss']
})
export class ProductItemComponent implements OnInit {

  //deklasati product
  public product: Product;

  //inicijalizovati sa default vrednostima
  constructor() {
    this.product = {
      name: "Dog food",
      imageURL: "https://vcahospitals.com/-/media/2/VCA/Images/LifeLearn-Images-Foldered/17507/Dog_Food_Choices.ashx",
      price: 300,
      isOnSale: true,
      quantity: 0
    }
  }

  ngOnInit(): void {
  }


  increaseQuantity(){
    this.product.quantity++;
  }

  decreaseQuantity(){
    if (this.product.quantity > 0){
      this.product.quantity--
    }
  }
}
