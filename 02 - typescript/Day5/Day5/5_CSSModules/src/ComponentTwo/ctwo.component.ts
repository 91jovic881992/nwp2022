import { Component } from '@angular/core';

var styles = require('./ctwo.component.css');

@Component({
    selector: 'ctwo',
    template: `
            <h2>CSS Modules - C2</h2>
            <h3 class="${styles.same}">Using CSS Class</h3>
        `
})
export class ComponentTwo { }
