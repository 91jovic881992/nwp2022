import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";

import { Hello1Component } from "./hello1.component";
import { Hello2Component } from "./hello2.component";

@NgModule({
    imports: [BrowserModule],
    declarations: [Hello1Component, Hello2Component],
    bootstrap: [Hello1Component, Hello2Component]
})
export class AppModule { } 