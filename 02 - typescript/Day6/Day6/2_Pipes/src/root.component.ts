import { Component, ViewChild } from "@angular/core";

@Component({
    selector: 'root',
    template: `
            <div class="container">     
                <lcomp></lcomp>

                <h3>{{name | capitalize}}</h3>

                <h3>{{name}}</h3>
                <h3>{{name | uppercase}}</h3>
                <h3>{{today}}</h3>
                <h3>{{today | date}}</h3>
                <h3>{{today | date:'MMM/dd/yyyy'}}</h3>
                <h3>{{today | date:format}}</h3>
                <button (click)=toggleFormat()>Toggle</button>
                <h3 class="text-right">Time is: {{forClock | date:'mediumTime'}}</h3>
            </div>
        `
})
export class RootComponent {
    name = "manish sharma";
    today = new Date();
    forClock: Date;
    toggle = true;

    constructor(){
        setInterval(() => { this.startTimer(); }, 1000);
    }

    private startTimer(){
        this.forClock = new Date();
    }

    get format() { return this.toggle ? 'shortDate' : 'fullDate'; }

    toggleFormat(){
        this.toggle = !this.toggle;
    }
}