// let area1 = function (h: number, w?: number) {
//     w = w || h;
//     return h * w;
// }
var p1 = {
    name: 'Manish',
    age: 34,
    greet: function (msg) {
        return "Hi There";
    }
};
var p2 = {
    name: 'Alex',
    age: 35,
    greet: function (m) {
        return "CYa";
    }
};
var Person = /** @class */ (function () {
    function Person() {
    }
    Person.prototype.greet = function (m) {
        return "Hello";
    };
    return Person;
}());
var p11 = new Person();
var p12 = new Person();
