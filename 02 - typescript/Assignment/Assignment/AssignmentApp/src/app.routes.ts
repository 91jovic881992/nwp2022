import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from "@angular/router";

import { PListComponent } from "./ListComponent/plist.component";
import { CreateProductComponent } from "./CreateProduct/createproduct.component";
import { EditProductComponent } from "./EditProduct/editProduct.component";

export const routes: Routes = [
    { path: '', component: PListComponent },
    { path: 'products/create', component: CreateProductComponent },
    { path: 'products/edit/:id', component: EditProductComponent }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes)