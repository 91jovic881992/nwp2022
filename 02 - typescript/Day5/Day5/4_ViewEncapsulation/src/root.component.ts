import { Component } from "@angular/core";

@Component({
    selector: 'root',
    template: `
            <div class="container">     
                <h1 class="text-danger">Root Component Loaded....</h1>
                <cone></cone>
                <ctwo></ctwo>
            </div>
        `
})
export class RootComponent { }