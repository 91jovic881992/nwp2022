import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";

import { RootComponent } from "./root.component";
import { PListComponent } from "./ListComponent/plist.component";
import { ProductComponent } from "./Shared/product.component";
import { CreateProductComponent } from "./CreateProduct/createproduct.component";
import { EditProductComponent } from "./EditProduct/editProduct.component";

import { routing } from "./app.routes";

@NgModule({
    imports: [BrowserModule, FormsModule, ReactiveFormsModule, HttpModule, routing],
    declarations: [RootComponent, PListComponent, ProductComponent, CreateProductComponent, EditProductComponent],
    bootstrap: [RootComponent]
})
export class AppModule { } 