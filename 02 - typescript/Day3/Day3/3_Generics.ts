// var arr = new Array<number>();

// class Queue {
//     private data: any[] = [];

//     push(d: any) {
//         this.data.push(d);
//     }

//     pop() {
//         return this.data.shift();
//     }
// }

class Queue<T> {
    private data: T[] = [];

    push(d: T) {
        this.data.push(d);
    }

    pop() {
        return this.data.shift();
    }
}

let q1 = new Queue<number>();
q1.push(10);
console.log(q1.pop().toFixed());

let q2 = new Queue<string>();
q2.push("ABC");
console.log(q2.pop().toUpperCase());

// ---------------------------------------------- Constraints
interface ILength{
    length: number;
}

function GetLength<T extends ILength>(arg: T) {
    return arg.length;
}

console.log(GetLength("Manish"));
// console.log(GetLength(10));
console.log(GetLength([10, 20, 30, 40, 50, 60]));