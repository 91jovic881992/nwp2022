import { Routes } from "@angular/router";

import { DefaultComponent } from "./default.component";
import { ListComponent } from './ListComponent/list.component';
import { QuoteComponent } from "./QuoteComponent/quote.component";

export const routes: Routes = [
    { path: '', component: DefaultComponent },
    { path: 'list', component: ListComponent },
    { path: 'quote', component: QuoteComponent },
    { path: 'quote/:name', component: QuoteComponent },
    { path: 'lazy', loadChildren: './LazyModule/app.lazymodule#LazyModule' }
]